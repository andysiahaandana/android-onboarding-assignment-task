package com.fernandocejas.android10.sample.domain.moviedb.model;

import java.util.List;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version VideoResult, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class VideoResult {

    private int id;

    private List<Video> videosList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Video> getVideosList() {
        return videosList;
    }

    public void setVideosList(
        List<Video> videosList) {
        this.videosList = videosList;
    }
}
