package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetPopularMoviesListUseCase extends UseCase<MovieResult,
    GetPopularMoviesListUseCase.Params> {

    private final MovieDBRepository movieDBRepository;

    @Inject
    GetPopularMoviesListUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<MovieResult> buildUseCaseObservable(Params params) {
        return this.movieDBRepository.getMoviePopularList(params.page);
    }

    public static class Params {

        private final int page;

        private Params(int page) {
            this.page = page;
        }

        public static Params forGetMovie(int page) {
            return new Params(page);
        }
    }
}
