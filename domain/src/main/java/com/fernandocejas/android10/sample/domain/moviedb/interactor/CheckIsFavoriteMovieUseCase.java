package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CheckIsFavoriteMovieUseCase extends UseCase<Boolean,
    CheckIsFavoriteMovieUseCase.Params> {

    private MovieDBRepository movieDBRepository;

    @Inject
    CheckIsFavoriteMovieUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable(
        CheckIsFavoriteMovieUseCase.Params params) {
        return movieDBRepository.checkIsFavoriteMovie(params.movie_id);
    }

    public static class Params {

        private int movie_id;

        private Params(int movie_id) {
            this.movie_id = movie_id;
        }

        public static CheckIsFavoriteMovieUseCase.Params forCheckIsFavoriteMovie(int movie_id) {
            return new CheckIsFavoriteMovieUseCase.Params(movie_id);
        }

    }
}
