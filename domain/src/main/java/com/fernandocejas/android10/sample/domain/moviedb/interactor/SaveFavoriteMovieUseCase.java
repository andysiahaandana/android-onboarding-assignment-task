package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SaveFavoriteMovieUseCase extends UseCase<Boolean, SaveFavoriteMovieUseCase.Params> {

    private MovieDBRepository movieDBRepository;

    @Inject
    SaveFavoriteMovieUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable(SaveFavoriteMovieUseCase.Params params) {
        return movieDBRepository
            .saveFavoriteMovie(params.movie_id, params.poster_path, params.isFavorite);
    }

    public static class Params {

        private int movie_id;

        private String poster_path;

        private Boolean isFavorite;

        private Params(int movie_id, String poster_path, Boolean isFavorite) {
            this.movie_id = movie_id;
            this.poster_path = poster_path;
            this.isFavorite = isFavorite;
        }

        public static SaveFavoriteMovieUseCase.Params forSaveFavoriteMovie(int movie_id,
            String poster_path,
            Boolean isFavorite) {
            return new SaveFavoriteMovieUseCase.Params(movie_id, poster_path, isFavorite);
        }

    }
}
