package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetDetailsMovieUseCase extends UseCase<Movie, GetDetailsMovieUseCase.Params> {

    private final MovieDBRepository movieDBRepository;

    @Inject
    GetDetailsMovieUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<Movie> buildUseCaseObservable(Params params) {
        return this.movieDBRepository.getDetailmovie(params.movieId);
    }

    public static class Params {

        private final int movieId;

        private Params(int movieId) {
            this.movieId = movieId;
        }

        public static Params forGetMovie(int movieId) {
            return new Params(movieId);
        }
    }
}
