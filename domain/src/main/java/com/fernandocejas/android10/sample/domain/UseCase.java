package com.fernandocejas.android10.sample.domain;

import com.fernandocejas.arrow.checks.Preconditions;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class UseCase<T, Params> {

    /** threadExecutor */
    private final ThreadExecutor threadExecutor;

    /** postExecutionThread */
    private final PostExecutionThread postExecutionThread;

    /** disposables */
    private final CompositeDisposable disposables;

    public UseCase(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        disposables = new CompositeDisposable();
    }

    /**
     * Builds an {@link Observable} which will be used when executing the current {@link UseCase}
     *
     * @param params Parameters (Optional) used to build/execute this use case.
     * @return Observable
     */
    protected abstract Observable<T> buildUseCaseObservable(Params params);

    /**
     * Execute the current use case.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     *                 by {@link #buildUseCaseObservable(Params)} ()} method.
     */
    public void execute(DisposableObserver<T> observer) {
        execute(observer, null);
    }

    /**
     * Execute the current use case.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     *                 by {@link #buildUseCaseObservable(Params)} ()} method.
     * @param params   Parameters (Optional) used to build/execute this use case.
     */
    public void execute(DisposableObserver<T> observer, Params params) {
        Preconditions.checkNotNull(observer);

        final Observable<T> observable = buildUseCaseObservable(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.getScheduler());

        addDisposable(observable.subscribeWith(observer));
    }

    /**
     * Execute the current use case in background thread.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     *                 by {@link #buildUseCaseObservable(Params)} ()} method.
     */
    public void executeInBackground(DisposableObserver<T> observer) {
        executeInBackground(observer, null);
    }

    /**
     * Execute the current use case in background thread.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     *                 by {@link #buildUseCaseObservable(Params)} ()} method.
     * @param params   Parameters (Optional) used to build/execute this use case.
     */
    public void executeInBackground(DisposableObserver<T> observer, Params params) {
        Preconditions.checkNotNull(observer);

        final Observable<T> observable = buildUseCaseObservable(params)
            .subscribeOn(Schedulers.from(threadExecutor));

        addDisposable(observable.subscribeWith(observer));
    }

    /**
     * Dispose from current {@link CompositeDisposable}
     */
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    /**
     * Dispose from current {@link CompositeDisposable}
     *
     * @param disposable {@link Disposable}
     */
    private void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(disposables);
        disposables.add(disposable);
    }
}