package com.fernandocejas.android10.sample.domain.moviedb.repository;

import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;

import java.util.List;

import io.reactivex.Observable;

public interface MovieDBRepository {

    Observable<MovieResult> getMoviePopularList(int page);

    Observable<MovieResult> getMovieTopRatedList(int page);

    Observable<Movie> getDetailmovie(final int movieId);

    Observable<VideoResult> getVideoByMovieId(final int movieId);

    Observable<Boolean> saveFavoriteMovie(int movie_id, String poster_path, Boolean isFavorite);

    Observable<Boolean> checkIsFavoriteMovie(int movieId);

    Observable<List<Movie>> getMovieFavoriteMovieList();
}
