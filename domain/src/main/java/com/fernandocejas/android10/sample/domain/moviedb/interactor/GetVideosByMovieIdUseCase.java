package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetVideosByMovieIdUseCase extends UseCase<VideoResult,
    GetVideosByMovieIdUseCase.Params> {

    private final MovieDBRepository movieDBRepository;

    @Inject
    GetVideosByMovieIdUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<VideoResult> buildUseCaseObservable(Params params) {
        return this.movieDBRepository.getVideoByMovieId(params.movieId);
    }

    public static class Params {

        private final int movieId;

        private Params(int movieId) {
            this.movieId = movieId;
        }

        public static Params forGetVideo(int movieId) {
            return new Params(movieId);
        }
    }
}
