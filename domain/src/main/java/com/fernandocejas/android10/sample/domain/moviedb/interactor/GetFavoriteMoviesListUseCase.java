
package com.fernandocejas.android10.sample.domain.moviedb.interactor;

import com.fernandocejas.android10.sample.domain.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.UseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetFavoriteMoviesListUseCase extends UseCase<List<Movie>, Void> {

    private final MovieDBRepository movieDBRepository;

    @Inject
    GetFavoriteMoviesListUseCase(MovieDBRepository movieDBRepository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.movieDBRepository = movieDBRepository;
    }

    @Override
    protected Observable<List<Movie>> buildUseCaseObservable(Void unused) {
        return this.movieDBRepository.getMovieFavoriteMovieList();
    }
}
