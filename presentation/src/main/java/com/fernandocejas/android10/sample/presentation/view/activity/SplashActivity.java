package com.fernandocejas.android10.sample.presentation.view.activity;

import com.fernandocejas.android10.sample.presentation.R;

import android.content.Intent;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version SplashActivity, v 0.1 2019-10-23 14:50 by Andy Leonard Siahaan
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Observable.timer(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(time -> {
                Intent intent = new Intent(this, MovieListActivity.class);
                intent.setData(getIntent().getData());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            });
    }

}
