package com.fernandocejas.android10.sample.presentation.view.fragment;

import com.bumptech.glide.Glide;
import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.components.MovieComponent;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;
import com.fernandocejas.android10.sample.presentation.presenter.MovieDetailsPresenter;
import com.fernandocejas.android10.sample.presentation.view.MovieDetailsView;
import com.fernandocejas.android10.sample.presentation.view.adapter.VideoAdapter;
import com.fernandocejas.arrow.checks.Preconditions;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MovieDetailsFragment extends BaseFragment implements MovieDetailsView {

    private static final String PARAM_MOVIE_ID = "param_movie_id";

    @Inject
    MovieDetailsPresenter movieDetailsPresenter;

    @Inject
    VideoAdapter videoAdapter;

    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.img_Poster)
    ImageView img_Poster;

    @BindView(R.id.txt_release_date)
    TextView txt_release_date;

    @BindView(R.id.txt_duration)
    TextView txt_duration;

    @BindView(R.id.txt_rate)
    TextView txt_rate;

    @BindView(R.id.txt_favorite)
    TextView txt_favorite;

    @BindView(R.id.txt_overview)
    TextView txt_overview;

    @BindView(R.id.videoList)
    RecyclerView rv_videoList;

    private VideoListListener videoListListener;

    private MovieModel current_movieModel;

    private VideoAdapter.OnItemClickListener onItemClickListener =
        videoModel -> {
            if (MovieDetailsFragment.this.movieDetailsPresenter != null && videoModel != null) {
                MovieDetailsFragment.this.movieDetailsPresenter.onVideoClicked(videoModel);
            }
        };

    public MovieDetailsFragment() {
        setRetainInstance(true);
    }

    public static MovieDetailsFragment forMovie(int movieId) {
        final MovieDetailsFragment movieDetailsFragment = new MovieDetailsFragment();
        final Bundle arguments = new Bundle();
        arguments.putInt(PARAM_MOVIE_ID, movieId);
        movieDetailsFragment.setArguments(arguments);
        return movieDetailsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MovieDetailsFragment.VideoListListener) {
            this.videoListListener = (MovieDetailsFragment.VideoListListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        this.getComponent(MovieComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle(getString(R.string.movie_detail));

        final View fragmentView = inflater
            .inflate(R.layout.fragment_movie_details, container, false);
        ButterKnife.bind(this, fragmentView);
        setupRecyclerView();
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.movieDetailsPresenter.setView(this);
        this.loadMovieDetails();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.movieDetailsPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.movieDetailsPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rv_videoList.setAdapter(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.videoListListener = null;
    }

    @Override
    public void showLoading() {
        this.getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        this.getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    private void loadMovieDetails() {
        if (this.movieDetailsPresenter != null) {
            this.movieDetailsPresenter.initialize(currentMovieId());
        }
    }

    private int currentMovieId() {
        final Bundle arguments = getArguments();
        Preconditions.checkNotNull(arguments, "Fragment arguments cannot be null");
        return arguments.getInt(PARAM_MOVIE_ID);
    }

    private void setupRecyclerView() {
        this.videoAdapter.setOnItemClickListener(onItemClickListener);
        this.rv_videoList.setLayoutManager(new LinearLayoutManager(context()));
        this.rv_videoList.setAdapter(videoAdapter);
        this.rv_videoList.setHasFixedSize(true);
        this.rv_videoList.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.txt_favorite)
    public void onClickFavorite() {
        this.movieDetailsPresenter
            .saveFavoriteMovie(current_movieModel.getId(), current_movieModel.getPoster_path(),
                !current_movieModel.isFavorite());
    }

    @Override
    public void onSuccessGetMovieDetail(MovieModel movieModel) {
        if (movieModel != null) {
            this.current_movieModel = movieModel;
            txt_title.setText(movieModel.getOriginal_title());
            Glide.with(getContext())
                .load("https://image.tmdb.org/t/p/w200" + movieModel.getPoster_path())
                .into(img_Poster);
            txt_release_date.setText(movieModel.getRelease_date());
            txt_duration.setText(movieModel.getRuntime() + " mins");
            txt_rate.setText(movieModel.getVote_average() + "/10");
            txt_overview.setText(movieModel.getOverview());
        }
    }

    @Override
    public void onSuccessGetVideos(List<VideoModel> videos) {
        this.videoAdapter.setVideoCollection(videos);
    }

    @Override
    public void viewVideo(VideoModel videoModel) {
        if (this.videoListListener != null) {
            this.videoListListener.onVideoClicked(videoModel);
        }
    }

    @Override
    public void onSuccessIsFavorite(Boolean isFavorite) {
        this.current_movieModel.setFavorite(isFavorite);
        if (isFavorite) {
            txt_favorite.setTag(true);
            txt_favorite.setText(getText(R.string.unmark_as_favorite));
        } else {
            txt_favorite.setTag(false);
            txt_favorite.setText(getText(R.string.mark_as_favorite));
        }
    }

    public interface VideoListListener {

        void onVideoClicked(final VideoModel movieModel);
    }
}
