package com.fernandocejas.android10.sample.presentation.view.activity;

import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.HasComponent;
import com.fernandocejas.android10.sample.presentation.internal.di.components.DaggerMovieComponent;
import com.fernandocejas.android10.sample.presentation.internal.di.components.MovieComponent;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;
import com.fernandocejas.android10.sample.presentation.view.fragment.MovieDetailsFragment;
import com.fernandocejas.android10.sample.presentation.view.fragment.MovieListFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class MovieListActivity extends BaseActivity implements HasComponent<MovieComponent>,
    MovieListFragment.MovieListListener, MovieDetailsFragment.VideoListListener {

    private MovieComponent movieComponent;

    private MovieListFragment movieListFragment = new MovieListFragment();

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, MovieListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_layout);

        this.initializeInjector();
        if (savedInstanceState == null) {
            addFragment(R.id.fragmentContainer, movieListFragment);
        }
    }

    private void initializeInjector() {
        this.movieComponent = DaggerMovieComponent.builder()
            .applicationComponent(getApplicationComponent())
            .activityModule(getActivityModule())
            .build();
    }

    @Override
    public MovieComponent getComponent() {
        return movieComponent;
    }

    @Override
    public void onMovieClicked(MovieModel movieModel) {
        replaceFragment(R.id.fragmentContainer, MovieDetailsFragment.forMovie(movieModel.getId()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void onVideoClicked(VideoModel movieModel) {
        this.navigator.navigateToYoutube(this, movieModel.getKey());
    }
}
