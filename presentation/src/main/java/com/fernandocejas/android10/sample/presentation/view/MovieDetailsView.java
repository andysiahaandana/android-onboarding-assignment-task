package com.fernandocejas.android10.sample.presentation.view;

import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;

import java.util.List;

public interface MovieDetailsView extends LoadDataView {

    void onSuccessGetMovieDetail(MovieModel movieModel);

    void onSuccessGetVideos(List<VideoModel> videos);

    void viewVideo(VideoModel videoModel);

    void onSuccessIsFavorite(Boolean isFavorite);
}
