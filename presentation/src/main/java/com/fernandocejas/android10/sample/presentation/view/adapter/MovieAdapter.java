package com.fernandocejas.android10.sample.presentation.view.adapter;

import com.bumptech.glide.Glide;
import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private final LayoutInflater layoutInflater;

    private List<MovieModel> moviesCollection;

    private OnItemClickListener onItemClickListener;

    private Context context;

    @Inject
    MovieAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.moviesCollection = Collections.emptyList();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_movie, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        final MovieModel movieModel = this.moviesCollection.get(position);
        if (movieModel != null) {
            if (movieModel.getPoster_path() != null)
                Glide.with(context)
                    .load("https://image.tmdb.org/t/p/w200" + movieModel.getPoster_path())
                    .into(holder.moviePoster);
            holder.itemView.setOnClickListener(v -> {
                if (MovieAdapter.this.onItemClickListener != null) {
                    MovieAdapter.this.onItemClickListener.onMovieItemClicked(movieModel);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (this.moviesCollection != null) ? this.moviesCollection.size() : 0;
    }

    public List<MovieModel> getMoviesCollection() {
        return moviesCollection;
    }

    public void setMoviesCollection(Collection<MovieModel> moviesCollection) {
        this.validateMoviesCollection(moviesCollection);
        this.moviesCollection = (List<MovieModel>) moviesCollection;
        this.notifyDataSetChanged();
    }

    public void showLoadMoreState(boolean enable) {
        if (enable) {
            moviesCollection.add(null);
            notifyDataSetChanged();
        } else {
            if (moviesCollection == null || moviesCollection.size() <= moviesCollection
                .size() - 1) {
                return;
            }

            moviesCollection.remove(moviesCollection.size() - 1);
            notifyItemRemoved(moviesCollection.size() - 1);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateMoviesCollection(Collection<MovieModel> moviesCollection) {
        if (moviesCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    public interface OnItemClickListener {

        void onMovieItemClicked(MovieModel movieModel);
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.moviePoster)
        ImageView moviePoster;

        MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
