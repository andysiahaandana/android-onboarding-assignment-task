package com.fernandocejas.android10.sample.presentation.view.activity;

import com.fernandocejas.android10.sample.presentation.R;

import android.os.Bundle;

import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);
        ButterKnife.bind(this);
    }
}
