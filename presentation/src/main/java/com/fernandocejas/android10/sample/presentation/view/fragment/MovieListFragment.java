package com.fernandocejas.android10.sample.presentation.view.fragment;

import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.components.MovieComponent;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.MovieResultModel;
import com.fernandocejas.android10.sample.presentation.presenter.MovieListPresenter;
import com.fernandocejas.android10.sample.presentation.view.MovieListView;
import com.fernandocejas.android10.sample.presentation.view.adapter.MovieAdapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListFragment extends BaseFragment implements MovieListView {

    @Inject
    MovieListPresenter movieListPresenter;

    @Inject
    MovieAdapter moviesAdapter;

    @BindView(R.id.rv_movieList)
    RecyclerView rv_movieList;

    @BindView(R.id.emptyView)
    TextView emptyView;

    private boolean hasMore;

    private boolean loading;

    private boolean firstPage;

    private MovieListListener movieListListener;

    private MovieAdapter.OnItemClickListener onItemClickListener =
        movieModel -> {
            if (MovieListFragment.this.movieListPresenter != null && movieModel != null) {
                MovieListFragment.this.movieListPresenter.onMovieClicked(movieModel);
            }
        };

    public MovieListFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MovieListListener) {
            this.movieListListener = (MovieListListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        this.getComponent(MovieComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        getActionBar().setDisplayHomeAsUpEnabled(false);

        switch (movieListPresenter.typeMenu) {
            case MovieListPresenter.TYPE_POPULAR:
                getActionBar().setTitle(getString(R.string.popular_movies));
                break;
            case MovieListPresenter.TYPE_TOPRATED:
                getActionBar().setTitle(getString(R.string.top_rated_movies));
                break;
            case MovieListPresenter.TYPE_FAVORITE:
                getActionBar().setTitle(getString(R.string.favorite_movies));
                break;
        }

        final View fragmentView = inflater.inflate(R.layout.fragment_movies_list, container, false);
        ButterKnife.bind(this, fragmentView);
        setupRecyclerView();
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.movieListPresenter.setView(this);
        this.loadMovieList(movieListPresenter.typeMenu);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.movieListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.movieListPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rv_movieList.setAdapter(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.movieListPresenter.destroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.movieListListener = null;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_popular:
                getActionBar().setTitle(getString(R.string.popular_movies));
                this.loadMovieList(movieListPresenter.TYPE_POPULAR);
                break;
            case R.id.action_toprated:
                getActionBar().setTitle(getString(R.string.top_rated_movies));
                loadMovieList(movieListPresenter.TYPE_TOPRATED);
                break;
            case R.id.action_favorite:
                getActionBar().setTitle(getString(R.string.favorite_movies));
                loadMovieList(movieListPresenter.TYPE_FAVORITE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private RecyclerView.OnScrollListener getOnScrollListener() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) rv_movieList.getLayoutManager();
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (layoutManager != null) {
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    if (shouldLoadMore(totalItemCount, lastVisibleItemPosition)) {
                        getLoadMoreLogic();
                    }
                }
            }
        };
    }

    private void getLoadMoreLogic() {
        loading = true;
        moviesAdapter.showLoadMoreState(true);
        movieListPresenter.getMovieList(true, movieListPresenter.getTypeMenu());
    }

    private boolean shouldLoadMore(int totalItemCount, int lastVisibleItemPosition) {
        return (hasMore && !firstPage && !loading && (totalItemCount <= lastVisibleItemPosition + 2));
    }

    @Override
    public void showLoading() {
        this.rv_movieList.setVisibility(View.VISIBLE);
        this.getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        this.getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
    }

    @Override
    public void hideRetry() {
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return this.getActivity().getApplicationContext();
    }

    @Override
    public void onSuccessGetMovieList(MovieResultModel movieResultModel) {
        this.moviesAdapter.setMoviesCollection(movieResultModel.getMovieList());
        checkDataIsEmpty(movieResultModel.getMovieList());
        if (movieResultModel.getPage() < movieResultModel.getTotal_pages()) {
            hasMore = true;
        } else {
            hasMore = false;
        }
        firstPage = false;
    }

    @Override
    public void onSuccessLoadMoreMovie(MovieResultModel movieResultModel) {
        moviesAdapter.showLoadMoreState(false);
        moviesAdapter.getMoviesCollection().addAll(movieResultModel.getMovieList());
        moviesAdapter.notifyDataSetChanged();
        if (movieResultModel.getPage() < movieResultModel.getTotal_pages()) {
            hasMore = true;
        } else {
            hasMore = false;
        }
        loading = false;
    }

    @Override
    public void onSuccessGetFavoriteMovieList(List<MovieModel> moviesFavorite) {
        this.moviesAdapter.setMoviesCollection(moviesFavorite);
        checkDataIsEmpty(moviesFavorite);
        hasMore = false;
        firstPage = false;
    }

    @Override
    public void viewMovie(MovieModel movieModel) {
        if (this.movieListListener != null) {
            this.movieListListener.onMovieClicked(movieModel);
        }
    }

    private void checkDataIsEmpty(List<MovieModel> movieList) {
        if (movieList.size() > 0) {
            rv_movieList.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            rv_movieList.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void setupRecyclerView() {
        this.moviesAdapter.setOnItemClickListener(onItemClickListener);
        this.rv_movieList.setLayoutManager(new GridLayoutManager(context(), 2));
        this.rv_movieList.setAdapter(moviesAdapter);
        this.rv_movieList.setHasFixedSize(true);
        this.rv_movieList.addOnScrollListener(getOnScrollListener());
    }

    public void loadMovieList(int type) {
        this.movieListPresenter.getMovieList(false, type);
    }

    public interface MovieListListener {

        void onMovieClicked(final MovieModel movieModel);
    }
}
