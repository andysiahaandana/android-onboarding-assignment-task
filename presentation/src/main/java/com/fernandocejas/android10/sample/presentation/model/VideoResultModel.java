package com.fernandocejas.android10.sample.presentation.model;

import java.util.List;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version VideoResultModel, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class VideoResultModel {

    private int id;

    private List<VideoModel> videosList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<VideoModel> getVideosList() {
        return videosList;
    }

    public void setVideosList(
        List<VideoModel> videosList) {
        this.videosList = videosList;
    }
}
