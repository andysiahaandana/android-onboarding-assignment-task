package com.fernandocejas.android10.sample.presentation.presenter;

import com.fernandocejas.android10.sample.domain.DefaultErrorBundle;
import com.fernandocejas.android10.sample.domain.DefaultObserver;
import com.fernandocejas.android10.sample.domain.ErrorBundle;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.GetFavoriteMoviesListUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.GetPopularMoviesListUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.GetTopRatedMoviesListUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;
import com.fernandocejas.android10.sample.presentation.exception.ErrorMessageFactory;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.mapper.MovieModelDataMapper;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.MovieResultModel;
import com.fernandocejas.android10.sample.presentation.view.MovieListView;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;

@PerActivity
public class MovieListPresenter implements Presenter {

    public static final int TYPE_POPULAR = 0;

    public static final int TYPE_TOPRATED = 1;

    public static final int TYPE_FAVORITE = 2;

    private final GetPopularMoviesListUseCase getPopularMovieListUseCase;

    private final GetTopRatedMoviesListUseCase getTopRatedMoviesListUseCase;

    private final GetFavoriteMoviesListUseCase getFavoriteMoviesListUseCase;

    private final MovieModelDataMapper movieModelDataMapper;

    public int typeMenu = TYPE_POPULAR;

    private int pageNumber = 1;

    private MovieListView viewListView;

    private DefaultObserver<MovieResult> getMovieResultListObserver;

    private DefaultObserver<List<Movie>> getMovieListFavoriteObserver;

    @Inject
    public MovieListPresenter(GetPopularMoviesListUseCase getPopularMovieListUseCase,
        GetTopRatedMoviesListUseCase getTopRatedMoviesListUseCase,
        GetFavoriteMoviesListUseCase getFavoriteMoviesListUseCase,
        MovieModelDataMapper movieModelDataMapper) {
        this.getPopularMovieListUseCase = getPopularMovieListUseCase;
        this.getTopRatedMoviesListUseCase = getTopRatedMoviesListUseCase;
        this.getFavoriteMoviesListUseCase = getFavoriteMoviesListUseCase;
        this.movieModelDataMapper = movieModelDataMapper;
    }

    public int getTypeMenu() {
        return typeMenu;
    }

    public void setView(@NonNull MovieListView view) {
        this.viewListView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getPopularMovieListUseCase.dispose();
        this.getTopRatedMoviesListUseCase.dispose();
        this.getFavoriteMoviesListUseCase.dispose();
        this.viewListView = null;
    }

    public void onMovieClicked(MovieModel movieModel) {
        this.viewListView.viewMovie(movieModel);
    }

    private void showViewLoading() {
        this.viewListView.showLoading();
    }

    private void hideViewLoading() {
        this.viewListView.hideLoading();
    }

    private void showViewRetry() {
        this.viewListView.showRetry();
    }

    private void hideViewRetry() {
        this.viewListView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
            errorBundle.getException());
        this.viewListView.showError(errorMessage);
    }

    private void showMovieCollectionInView(MovieResult movieResult) {
        final MovieResultModel movieModelsCollection = this.movieModelDataMapper
            .transform(movieResult);

        if (pageNumber > 1) {
            this.viewListView.onSuccessLoadMoreMovie(movieModelsCollection);
        } else {
            this.viewListView.onSuccessGetMovieList(movieModelsCollection);
        }
    }

    private void showMovieCollectionFavoriteInView(List<Movie> movies) {
        final List<MovieModel> movieModel = this.movieModelDataMapper.toMovie(movies);
        this.viewListView.onSuccessGetFavoriteMovieList(movieModel);
    }

    public void getMovieList(boolean loadMoreStatus, int typeMenu) {
        this.typeMenu = typeMenu;
        if (loadMoreStatus) {
            pageNumber++;
        } else {
            pageNumber = 1;
            viewListView.showLoading();
        }
        unsubscribeObserver();

        if (typeMenu == TYPE_POPULAR) {
            getPopularMovieListUseCase.execute(getMovieResultListObserver(loadMoreStatus),
                GetPopularMoviesListUseCase.Params.forGetMovie(pageNumber));
        } else if (typeMenu == TYPE_TOPRATED) {
            getTopRatedMoviesListUseCase.execute(getMovieResultListObserver(loadMoreStatus),
                GetTopRatedMoviesListUseCase.Params.forGetMovie(pageNumber));
        } else if (typeMenu == TYPE_FAVORITE) {
            getFavoriteMoviesListUseCase.execute(getMovieListFavoriteObserver(), null);
        }
    }

    private void showErrorGetMovie(boolean loadMoreStatus) {
        if (loadMoreStatus) {
            pageNumber--;
            viewListView.showError("Error when load more");
        } else {
            viewListView.showError("Error when load more");
        }
    }

    private DefaultObserver<MovieResult> getMovieResultListObserver(boolean loadMoreStatus) {
        if (getMovieResultListObserver == null) {
            getMovieResultListObserver = new DefaultObserver<MovieResult>() {
                @Override
                public void onNext(MovieResult movieResult) {
                    if (movieResult == null || movieResult.getMovieList() == null) {
                        MovieListPresenter.this.showErrorGetMovie(loadMoreStatus);
                    } else {
                        MovieListPresenter.this.showMovieCollectionInView(movieResult);
                    }
                }

                @Override
                public void onComplete() {
                    MovieListPresenter.this.hideViewLoading();
                }

                @Override
                public void onError(Throwable e) {
                    MovieListPresenter.this.hideViewLoading();
                    MovieListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
                    MovieListPresenter.this.showViewRetry();
                }
            };
        }

        return getMovieResultListObserver;
    }

    private DefaultObserver<List<Movie>> getMovieListFavoriteObserver() {
        if (getMovieListFavoriteObserver == null) {
            getMovieListFavoriteObserver = new DefaultObserver<List<Movie>>() {
                @Override
                public void onNext(List<Movie> movies) {
                    if (movies == null) {
                        MovieListPresenter.this.showErrorGetMovie(false);
                    } else {
                        MovieListPresenter.this.showMovieCollectionFavoriteInView(movies);
                    }
                }

                @Override
                public void onComplete() {
                    MovieListPresenter.this.hideViewLoading();
                }

                @Override
                public void onError(Throwable e) {
                    MovieListPresenter.this.hideViewLoading();
                    MovieListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
                    MovieListPresenter.this.showViewRetry();
                }
            };
        }

        return getMovieListFavoriteObserver;
    }

    private void unsubscribeObserver() {
        if (getMovieResultListObserver != null) {
            getMovieResultListObserver.dispose();
            getMovieResultListObserver = null;
        }

        if (getMovieListFavoriteObserver != null) {
            getMovieListFavoriteObserver.dispose();
            getMovieListFavoriteObserver = null;
        }
    }
}
