package com.fernandocejas.android10.sample.presentation.view;

import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.MovieResultModel;

import java.util.List;

public interface MovieListView extends LoadDataView {

    void onSuccessGetMovieList(MovieResultModel movieResultModel);

    void onSuccessLoadMoreMovie(MovieResultModel movieResultModel);

    void onSuccessGetFavoriteMovieList(List<MovieModel> moviesFavorite);

    void viewMovie(MovieModel movieModel);
}
