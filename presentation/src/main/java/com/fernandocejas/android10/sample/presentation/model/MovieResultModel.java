package com.fernandocejas.android10.sample.presentation.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version MovieResultModel, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class MovieResultModel {

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("results")
    private List<MovieModel> movieList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<MovieModel> getMovieList() {
        return movieList;
    }

    public void setMovieList(
        List<MovieModel> movieList) {
        this.movieList = movieList;
    }

}
