package com.fernandocejas.android10.sample.presentation.mapper;

import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.MovieResultModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class MovieModelDataMapper {

    @Inject
    MovieModelDataMapper() {
    }

    public MovieResultModel transform(MovieResult movieResult) {
        if (movieResult == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }

        final MovieResultModel movieResultModel = new MovieResultModel();
        movieResultModel.setPage(movieResult.getPage());
        movieResultModel.setTotal_results(movieResult.getTotal_results());
        movieResultModel.setTotal_pages(movieResult.getTotal_pages());
        movieResultModel.setMovieList(toMovie(movieResult.getMovieList()));

        return movieResultModel;
    }

    public List<MovieModel> toMovie(List<Movie> oldItem) {
        List<MovieModel> movieList = new ArrayList<>();

        for (Movie movie : oldItem) {
            movieList.add(transform(movie));
        }

        return movieList;
    }

    public MovieModel transform(Movie oldItem) {
        MovieModel movie = new MovieModel(oldItem.getId(), oldItem.getPoster_path());

        movie.setOriginal_language(oldItem.getOriginal_language());
        movie.setVote_average(oldItem.getVote_average());
        movie.setRelease_date(oldItem.getRelease_date());
        movie.setOverview(oldItem.getOverview());
        movie.setOriginal_title(oldItem.getOriginal_title());
        movie.setBackdrop_path(oldItem.getBackdrop_path());
        movie.setRuntime(oldItem.getRuntime());

        return movie;
    }
}
