package com.fernandocejas.android10.sample.presentation.mapper;

import com.fernandocejas.android10.sample.domain.moviedb.model.Video;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;
import com.fernandocejas.android10.sample.presentation.model.VideoResultModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class VideoModelDataMapper {

    @Inject
    VideoModelDataMapper() {
    }

    public VideoResultModel transform(VideoResult videoResult) {
        VideoResultModel videoResultModel = null;
        if (videoResult != null) {
            videoResultModel = new VideoResultModel();
            videoResultModel.setId(videoResult.getId());
            videoResultModel.setVideosList(toVideo(videoResult.getVideosList()));
        }
        return videoResultModel;
    }

    public List<VideoModel> toVideo(List<Video> oldItem) {
        List<VideoModel> videoList = new ArrayList<>();

        for (Video movie : oldItem) {
            videoList.add(transform(movie));
        }

        return videoList;
    }

    public VideoModel transform(Video oldItem) {
        VideoModel videoModel = new VideoModel();
        videoModel.setId(oldItem.getId());
        videoModel.setKey(oldItem.getKey());
        videoModel.setName(oldItem.getName());
        videoModel.setSite(oldItem.getSite());
        return videoModel;
    }
}
