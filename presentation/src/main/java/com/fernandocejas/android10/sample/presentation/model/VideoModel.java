package com.fernandocejas.android10.sample.presentation.model;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version VideoModel, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class VideoModel {

    private String id;

    private String key;

    private String name;

    private String site;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
