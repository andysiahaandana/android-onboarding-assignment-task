package com.fernandocejas.android10.sample.presentation.presenter;

import com.fernandocejas.android10.sample.domain.DefaultErrorBundle;
import com.fernandocejas.android10.sample.domain.DefaultObserver;
import com.fernandocejas.android10.sample.domain.ErrorBundle;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.CheckIsFavoriteMovieUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.GetDetailsMovieUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.GetVideosByMovieIdUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.interactor.SaveFavoriteMovieUseCase;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.Video;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;
import com.fernandocejas.android10.sample.presentation.exception.ErrorMessageFactory;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.mapper.MovieModelDataMapper;
import com.fernandocejas.android10.sample.presentation.mapper.VideoModelDataMapper;
import com.fernandocejas.android10.sample.presentation.model.MovieModel;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;
import com.fernandocejas.android10.sample.presentation.view.MovieDetailsView;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;

@PerActivity
public class MovieDetailsPresenter implements Presenter {

    private final GetDetailsMovieUseCase getDetailsMovieUseCase;

    private final GetVideosByMovieIdUseCase getVideosByMovieIdUseCase;

    private final MovieModelDataMapper movieModelDataMapper;

    private final VideoModelDataMapper videoModelDataMapper;

    private final SaveFavoriteMovieUseCase saveFavoriteMovieUseCase;

    private final CheckIsFavoriteMovieUseCase checkIsFavoriteMovieUseCase;

    private MovieDetailsView viewDetailsView;

    @Inject
    public MovieDetailsPresenter(GetDetailsMovieUseCase getDetailsMovieUseCase,
        GetVideosByMovieIdUseCase getVideosByMovieIdUseCase,
        MovieModelDataMapper movieModelDataMapper, VideoModelDataMapper videoModelDataMapper,
        SaveFavoriteMovieUseCase saveFavoriteMovieUseCase,
        CheckIsFavoriteMovieUseCase checkIsFavoriteMovieUseCase) {
        this.getDetailsMovieUseCase = getDetailsMovieUseCase;
        this.getVideosByMovieIdUseCase = getVideosByMovieIdUseCase;
        this.saveFavoriteMovieUseCase = saveFavoriteMovieUseCase;
        this.checkIsFavoriteMovieUseCase = checkIsFavoriteMovieUseCase;
        this.movieModelDataMapper = movieModelDataMapper;
        this.videoModelDataMapper = videoModelDataMapper;
    }

    public void onVideoClicked(VideoModel videoModel) {
        this.viewDetailsView.viewVideo(videoModel);
    }

    public void setView(@NonNull MovieDetailsView view) {
        this.viewDetailsView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getDetailsMovieUseCase.dispose();
        this.getVideosByMovieIdUseCase.dispose();
        this.saveFavoriteMovieUseCase.dispose();
        this.checkIsFavoriteMovieUseCase.dispose();
        this.viewDetailsView = null;
    }

    public void initialize(int movieId) {
        this.hideViewRetry();
        this.showViewLoading();
        this.getMovieDetails(movieId);
        this.getVideosById(movieId);
    }

    private void getMovieDetails(int movieId) {
        this.getDetailsMovieUseCase
            .execute(new MovieDetailsObserver(), GetDetailsMovieUseCase.Params
                .forGetMovie(movieId));
    }

    private void getVideosById(int movieId) {
        this.getVideosByMovieIdUseCase
            .execute(new VideoResultObserver(), GetVideosByMovieIdUseCase.Params
                .forGetVideo(movieId));
    }

    public void saveFavoriteMovie(int movie_id, String poster_path, Boolean isFavorite) {
        this.saveFavoriteMovieUseCase.execute(new DefaultObserver<Boolean>() {
            @Override
            public void onNext(Boolean success) {
                viewDetailsView.onSuccessIsFavorite(isFavorite);
            }
        }, SaveFavoriteMovieUseCase.Params
            .forSaveFavoriteMovie(movie_id, poster_path, isFavorite));
    }

    public void checkIsFavoriteMovie(int movieId) {
        this.checkIsFavoriteMovieUseCase.execute(new DefaultObserver<Boolean>() {
            @Override
            public void onNext(Boolean success) {
                viewDetailsView.onSuccessIsFavorite(success);
            }
        }, CheckIsFavoriteMovieUseCase.Params.forCheckIsFavoriteMovie(movieId));
    }

    private void showViewLoading() {
        this.viewDetailsView.showLoading();
    }

    private void hideViewLoading() {
        this.viewDetailsView.hideLoading();
    }

    private void showViewRetry() {
        this.viewDetailsView.showRetry();
    }

    private void hideViewRetry() {
        this.viewDetailsView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewDetailsView.context(),
            errorBundle.getException());
        this.viewDetailsView.showError(errorMessage);
    }

    private void showMovieDetailsInView(Movie movie) {
        final MovieModel movieModel = this.movieModelDataMapper.transform(movie);
        this.viewDetailsView.onSuccessGetMovieDetail(movieModel);
        checkIsFavoriteMovie(movie.getId());
    }

    private void showVideoListInView(List<Video> videos) {
        final List<VideoModel> videosModel = this.videoModelDataMapper.toVideo(videos);
        this.viewDetailsView.onSuccessGetVideos(videosModel);
    }

    private final class MovieDetailsObserver extends DefaultObserver<Movie> {

        @Override
        public void onNext(Movie movie) {
            MovieDetailsPresenter.this.showMovieDetailsInView(movie);
        }

        @Override
        public void onComplete() {
            MovieDetailsPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            MovieDetailsPresenter.this.hideViewLoading();
            MovieDetailsPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            MovieDetailsPresenter.this.showViewRetry();
        }
    }

    private final class VideoResultObserver extends DefaultObserver<VideoResult> {

        @Override
        public void onNext(VideoResult videoResult) {
            MovieDetailsPresenter.this.showVideoListInView(videoResult.getVideosList());
        }

        @Override
        public void onComplete() {
            MovieDetailsPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            MovieDetailsPresenter.this.hideViewLoading();
            MovieDetailsPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            MovieDetailsPresenter.this.showViewRetry();
        }
    }
}
