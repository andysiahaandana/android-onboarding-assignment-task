package com.fernandocejas.android10.sample.presentation.view.adapter;

import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.model.VideoModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    private final LayoutInflater layoutInflater;

    private List<VideoModel> videoCollection;

    private OnItemClickListener onItemClickListener;

    private Context context;

    @Inject
    VideoAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.videoCollection = Collections.emptyList();
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_trailers, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, final int position) {
        final VideoModel videoModel = this.videoCollection.get(position);
        if (videoModel != null) {
            holder.txt_title.setText(videoModel.getName());
            holder.itemView.setOnClickListener(v -> {
                if (VideoAdapter.this.onItemClickListener != null) {
                    VideoAdapter.this.onItemClickListener.onVideoItemClicked(videoModel);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (this.videoCollection != null) ? this.videoCollection.size() : 0;
    }

    public void setVideoCollection(Collection<VideoModel> videoCollection) {
        this.validateVideosCollection(videoCollection);
        this.videoCollection = (List<VideoModel>) videoCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateVideosCollection(Collection<VideoModel> videoCollection) {
        if (this.videoCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    public interface OnItemClickListener {

        void onVideoItemClicked(VideoModel videoModel);
    }

    static class VideoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title)
        TextView txt_title;

        VideoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
