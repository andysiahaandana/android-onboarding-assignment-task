package com.fernandocejas.android10.sample.data.moviedb.repository;

import com.fernandocejas.android10.sample.data.Source;
import com.fernandocejas.android10.sample.data.moviedb.mapper.MovieEntityDataMapper;
import com.fernandocejas.android10.sample.data.moviedb.mapper.VideosEntityDataMapper;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.repository.datasource.MovieDataStore;
import com.fernandocejas.android10.sample.data.moviedb.repository.datasource.MovieDataStoreFactory;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;
import com.fernandocejas.android10.sample.domain.moviedb.repository.MovieDBRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

@Singleton
public class MovieDataEntityDBRepository implements MovieDBRepository {

    private final MovieDataStoreFactory movieDataStoreFactory;

    private final MovieEntityDataMapper movieEntityDataMapper;

    private final VideosEntityDataMapper videosEntityDataMapper;

    @Inject
    MovieDataEntityDBRepository(MovieDataStoreFactory dataStoreFactory,
        MovieEntityDataMapper movieEntityDataMapper,
        VideosEntityDataMapper videosEntityDataMapper) {
        this.movieDataStoreFactory = dataStoreFactory;
        this.movieEntityDataMapper = movieEntityDataMapper;
        this.videosEntityDataMapper = videosEntityDataMapper;
    }

    private MovieDataStore createContactFavoriteMovieData() {
        return movieDataStoreFactory.createData(Source.LOCAL);
    }

    @Override
    public Observable<MovieResult> getMoviePopularList(int page) {
        final MovieDataStore movieDataStore = this.movieDataStoreFactory.createData(Source.NETWORK);
        return movieDataStore.moviePopularList(page).map(this.movieEntityDataMapper::transform);
    }

    @Override
    public Observable<MovieResult> getMovieTopRatedList(int page) {
        final MovieDataStore movieDataStore = this.movieDataStoreFactory.createData(Source.NETWORK);
        return movieDataStore.movieTopRatedList(page).map(this.movieEntityDataMapper::transform);
    }

    @Override
    public Observable<Movie> getDetailmovie(int movieId) {
        final MovieDataStore movieDataStore = this.movieDataStoreFactory.createData(Source.NETWORK);
        return movieDataStore.movieDetailsById(movieId).map(this.movieEntityDataMapper::transform);
    }

    @Override
    public Observable<VideoResult> getVideoByMovieId(int movieId) {
        final MovieDataStore movieDataStore = this.movieDataStoreFactory.createData(Source.NETWORK);
        return movieDataStore.videosByMovieId(movieId).map(this.videosEntityDataMapper::transform);
    }

    @Override
    public Observable<Boolean> saveFavoriteMovie(int movie_id, String poster_path,
        Boolean isFavorite) {
        MovieEntity favoriteMovieEntityData = new MovieEntity();
        favoriteMovieEntityData.setId(movie_id);
        favoriteMovieEntityData.setPoster_path(poster_path);

        if (isFavorite) {
            //save to localDB
            return createContactFavoriteMovieData().saveFavoriteMovie(favoriteMovieEntityData)
                .flatMap(
                    (Function<Long, ObservableSource<Boolean>>) movieId -> {
                        if (movieId != null) {
                            return Observable.just(true);
                        }
                        return Observable.just(false);
                    });
        } else {
            //remove from localDb
            return createContactFavoriteMovieData().deleteFavoriteMovie(favoriteMovieEntityData);
        }
    }

    @Override
    public Observable<Boolean> checkIsFavoriteMovie(int movieId) {
        return createContactFavoriteMovieData().checkIsFavoriteMovie(movieId);
    }

    @Override
    public Observable<List<Movie>> getMovieFavoriteMovieList() {
        return createContactFavoriteMovieData().getListFavoriteMovie()
            .map(movieEntityDataMapper::toMovie);
    }
}
