package com.fernandocejas.android10.sample.data.base;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.repository.datasource.persistence.dao.FavoriteMovieDao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {MovieEntity.class}, version = 1, exportSchema = false)
public abstract class BasePersistanceDao extends RoomDatabase {

    public abstract FavoriteMovieDao favoriteMovieDao();

}
