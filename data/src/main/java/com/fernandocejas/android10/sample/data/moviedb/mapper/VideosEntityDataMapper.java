package com.fernandocejas.android10.sample.data.moviedb.mapper;

import com.fernandocejas.android10.sample.data.moviedb.model.VideoEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;
import com.fernandocejas.android10.sample.domain.moviedb.model.Video;
import com.fernandocejas.android10.sample.domain.moviedb.model.VideoResult;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class VideosEntityDataMapper {

    @Inject
    VideosEntityDataMapper() {
    }

    public VideoResult transform(VideoResultEntity videoResultEntity) {
        VideoResult videoResult = null;
        if (videoResultEntity != null) {
            videoResult = new VideoResult();
            videoResult.setId(videoResultEntity.getId());
            videoResult.setVideosList(toVideo(videoResultEntity.getVideosList()));
        }
        return videoResult;
    }

    private List<Video> toVideo(List<VideoEntity> oldItem) {
        List<Video> videoList = new ArrayList<>();

        for (VideoEntity movie : oldItem) {
            videoList.add(transform(movie));
        }

        return videoList;
    }

    public Video transform(VideoEntity oldItem) {
        Video video = new Video();
        video.setId(oldItem.getId());
        video.setKey(oldItem.getKey());
        video.setName(oldItem.getName());
        video.setSite(oldItem.getSite());
        return video;
    }
}
