package com.fernandocejas.android10.sample.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.StringDef;

@StringDef({Source.LOCAL, Source.NETWORK, Source.MOCK})
@Retention(RetentionPolicy.SOURCE)
public @interface Source {

    String LOCAL = "local";

    String NETWORK = "network";

    String MOCK = "mock";
}
