package com.fernandocejas.android10.sample.data.moviedb.mapper;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.domain.moviedb.model.Movie;
import com.fernandocejas.android10.sample.domain.moviedb.model.MovieResult;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovieEntityDataMapper {

    @Inject
    MovieEntityDataMapper() {
    }

    public MovieResult transform(MovieResultEntity movieResultEntity) {
        MovieResult movieResult = null;
        if (movieResultEntity != null) {
            movieResult = new MovieResult();
            movieResult.setPage(movieResultEntity.getPage());
            movieResult.setTotal_results(movieResultEntity.getTotal_results());
            movieResult.setTotal_pages(movieResultEntity.getTotal_pages());
            movieResult.setMovieList(toMovie(movieResultEntity.getMovies()));
        }
        return movieResult;
    }

    public List<Movie> toMovie(List<MovieEntity> oldItem) {
        List<Movie> movieList = new ArrayList<>();

        for (MovieEntity movie : oldItem) {
            movieList.add(transform(movie));
        }

        return movieList;
    }

    public Movie transform(MovieEntity oldItem) {
        Movie movie = new Movie(oldItem.getId(), oldItem.getPoster_path());

        movie.setOriginal_language(oldItem.getOriginal_language());
        movie.setVote_average(oldItem.getVote_average());
        movie.setRelease_date(oldItem.getRelease_date());
        movie.setOverview(oldItem.getOverview());
        movie.setOriginal_title(oldItem.getOriginal_title());
        movie.setBackdrop_path(oldItem.getBackdrop_path());
        movie.setRuntime(oldItem.getRuntime());

        return movie;
    }
}
