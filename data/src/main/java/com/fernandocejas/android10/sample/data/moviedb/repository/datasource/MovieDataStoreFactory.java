package com.fernandocejas.android10.sample.data.moviedb.repository.datasource;

import com.fernandocejas.android10.sample.data.AbstractEntityDataFactory;
import com.fernandocejas.android10.sample.data.Source;
import com.fernandocejas.android10.sample.data.moviedb.mapper.MovieDetailsJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.mapper.MovieEntityJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.mapper.VideosEntityJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.repository.datasource.persistence.PersistenceFavoriteMovieEntityData;
import com.fernandocejas.android10.sample.data.net.MovieRestApi;
import com.fernandocejas.android10.sample.data.net.MovieRestApiImpl;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;

@Singleton
public class MovieDataStoreFactory extends AbstractEntityDataFactory<MovieDataStore> {

    private final Context context;

    private final PersistenceFavoriteMovieEntityData persistenceFavoriteMovieEntityData;

    @Inject
    MovieDataStoreFactory(@NonNull Context context, @NonNull
        PersistenceFavoriteMovieEntityData persistenceRecentContactEntityData) {
        this.context = context.getApplicationContext();
        this.persistenceFavoriteMovieEntityData = persistenceRecentContactEntityData;
    }

    private MovieDataStore createCloudDataStore() {
        final MovieEntityJsonMapper movieEntityJsonMapper = new MovieEntityJsonMapper();
        final MovieDetailsJsonMapper movieDetailsJsonMapper = new MovieDetailsJsonMapper();
        final VideosEntityJsonMapper videosEntityDataMapper = new VideosEntityJsonMapper();

        final MovieRestApi movieRestApi = new MovieRestApiImpl(this.context, movieEntityJsonMapper,
            movieDetailsJsonMapper, videosEntityDataMapper);

        return new CloudMovieDataStore(movieRestApi);
    }

    @Override
    public MovieDataStore createData(String source) {
        switch (source) {
            case Source.LOCAL:
                return persistenceFavoriteMovieEntityData;
            default:
                return createCloudDataStore();
        }
    }
}
