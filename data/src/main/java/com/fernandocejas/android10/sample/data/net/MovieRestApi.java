/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.net;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;
import com.fernandocejas.android10.sample.data.user.model.UserEntity;

import io.reactivex.Observable;

/**
 * UserRestApi for retrieving data from the network.
 */
public interface MovieRestApi {

    String API_BASE_URL = "https://api.themoviedb.org/3/";

    String API_URL_GET_POPULAR_LIST = API_BASE_URL + "movie/popular";

    String API_URL_GET_TOP_RATED_LIST = API_BASE_URL + "movie/top_rated";

    String API_URL_GET_DETAILS_MOVIE = API_BASE_URL + "movie/";

    /**
     * Retrieves an {@link Observable} which will emit a List of {@link UserEntity}.
     */
    Observable<MovieResultEntity> moviePopularList(int page);

    Observable<MovieResultEntity> movieTopRatedList(int page);

    Observable<MovieResultEntity> movieFavoriteList();

    /**
     * Retrieves an {@link Observable} which will emit a {@link UserEntity}.
     *
     * @param movieId The user id used to get user data.
     */
    Observable<MovieEntity> movieDetailsById(final int movieId);

    Observable<VideoResultEntity> videosMovieById(final int movieId);
}
