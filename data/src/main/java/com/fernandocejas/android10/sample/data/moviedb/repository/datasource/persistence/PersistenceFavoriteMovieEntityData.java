package com.fernandocejas.android10.sample.data.moviedb.repository.datasource.persistence;

import com.fernandocejas.android10.sample.data.base.BasePersistence;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.repository.datasource.MovieDataStore;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class PersistenceFavoriteMovieEntityData extends BasePersistence implements MovieDataStore {

    @Inject
    public PersistenceFavoriteMovieEntityData(Context context) {
        super(context);
    }

    @Override
    public Observable<MovieResultEntity> moviePopularList(int page) {
        return null;
    }

    @Override
    public Observable<MovieResultEntity> movieTopRatedList(int page) {
        return null;
    }

    @Override
    public Observable<MovieEntity> movieDetailsById(int movieId) {
        return null;
    }

    @Override
    public Observable<VideoResultEntity> videosByMovieId(int movieId) {
        return null;
    }

    @Override
    public Observable<Long> saveFavoriteMovie(MovieEntity newMovieFavoriteEntity) {
        return Observable.defer(() -> {
            MovieEntity movieEntity = getSingleMovieFavorite(newMovieFavoriteEntity.getId());

            if (isFavoriteMovieExist(movieEntity)) {
                return Observable.just((long) getDb().favoriteMovieDao()
                    .updateMovieEntity(movieEntity));
            } else {
                return Observable
                    .just(getDb().favoriteMovieDao().insertFavoriteMovie(newMovieFavoriteEntity));
            }
        });
    }

    @Override
    public Observable<Boolean> deleteFavoriteMovie(MovieEntity deletedMovieFavoriteEntity) {
        return Observable.defer(() -> {
            getDb().favoriteMovieDao().deleteFavoriteMovie(deletedMovieFavoriteEntity.getId());
            return Observable.just(true);
        });
    }

    @Override
    public Observable<Boolean> checkIsFavoriteMovie(int movieId) {
        MovieEntity movieEntity = getDb().favoriteMovieDao().getMovieEntityById(movieId);
        if (isFavoriteMovieExist(movieEntity)) {
            return Observable.just(true);
        } else {
            return Observable.just(false);
        }
    }

    @Override
    public Observable<List<MovieEntity>> getListFavoriteMovie() {
        return Observable.defer(
            () -> Observable.just(getDb().favoriteMovieDao().getMovieEntity()));
    }

    private MovieEntity getSingleMovieFavorite(int id) {
        return getDb().favoriteMovieDao().getMovieEntityById(id);
    }

    private boolean isFavoriteMovieExist(MovieEntity movieEntity) {
        return movieEntity != null;
    }
}