package com.fernandocejas.android10.sample.data.moviedb.repository.datasource.persistence.dao;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface FavoriteMovieDao {

    @Insert
    Long insertFavoriteMovie(MovieEntity newMovieFavoriteEntity);

    @Query("SELECT * FROM MovieEntity WHERE id = :movieId")
    MovieEntity getMovieEntityById(int movieId);

    @Query("Delete FROM MovieEntity where id = :movieId")
    void deleteFavoriteMovie(int movieId);

    @Update
    int updateMovieEntity(MovieEntity movieEntity);

    @Query("SELECT * FROM MovieEntity")
    List<MovieEntity> getMovieEntity();

}
