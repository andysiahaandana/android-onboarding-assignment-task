package com.fernandocejas.android10.sample.data.moviedb.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class MovieEntityJsonMapper {

    private final Gson gson;

    @Inject
    public MovieEntityJsonMapper() {
        this.gson = new Gson();
    }

    public MovieResultEntity transformMovieResultEntity(
        String movieJsonResponse) throws JsonSyntaxException {
        final Type movieResultEntity = new TypeToken<MovieResultEntity>() {
        }.getType();
        return this.gson.fromJson(movieJsonResponse, movieResultEntity);
    }
}
