package com.fernandocejas.android10.sample.data.moviedb.repository.datasource;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;
import com.fernandocejas.android10.sample.data.net.MovieRestApi;

import java.util.List;

import io.reactivex.Observable;

class CloudMovieDataStore implements MovieDataStore {

    private final MovieRestApi movieRestApi;

    CloudMovieDataStore(MovieRestApi movieRestApi) {
        this.movieRestApi = movieRestApi;
    }

    @Override
    public Observable<MovieResultEntity> moviePopularList(int page) {
        return this.movieRestApi.moviePopularList(page);
    }

    @Override
    public Observable<MovieResultEntity> movieTopRatedList(int page) {
        return this.movieRestApi.movieTopRatedList(page);
    }

    @Override
    public Observable<MovieEntity> movieDetailsById(int movieId) {
        return this.movieRestApi.movieDetailsById(movieId);
    }

    @Override
    public Observable<VideoResultEntity> videosByMovieId(int movieId) {
        return this.movieRestApi.videosMovieById(movieId);
    }

    @Override
    public Observable<Long> saveFavoriteMovie(MovieEntity MovieEntity) {
        return null;
    }

    @Override
    public Observable<Boolean> deleteFavoriteMovie(MovieEntity movieEntity) {
        return null;
    }

    @Override
    public Observable<Boolean> checkIsFavoriteMovie(int movieId) {
        return null;
    }

    @Override
    public Observable<List<MovieEntity>> getListFavoriteMovie() {
        return null;
    }
}
