package com.fernandocejas.android10.sample.data.moviedb.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version MovieResultEntity, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class VideoEntity {

    @SerializedName("id")
    private String id;

    @SerializedName("key")
    private String key;

    @SerializedName("name")
    private String name;

    @SerializedName("site")
    private String site;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getSite() {
        return site;
    }
}
