package com.fernandocejas.android10.sample.data.moviedb.repository.datasource;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;

import java.util.List;

import io.reactivex.Observable;

public interface MovieDataStore {

    Observable<MovieResultEntity> moviePopularList(int page);

    Observable<MovieResultEntity> movieTopRatedList(int page);

    Observable<MovieEntity> movieDetailsById(final int movieId);

    Observable<VideoResultEntity> videosByMovieId(final int movieId);

    Observable<Long> saveFavoriteMovie(MovieEntity movieEntity);

    Observable<Boolean> deleteFavoriteMovie(MovieEntity movieEntity);

    Observable<Boolean> checkIsFavoriteMovie(int movieId);

    Observable<List<MovieEntity>> getListFavoriteMovie();
}
