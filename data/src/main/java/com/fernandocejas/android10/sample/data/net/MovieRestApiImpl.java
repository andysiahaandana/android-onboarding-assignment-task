/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.net;

import com.fernandocejas.android10.sample.data.exception.NetworkConnectionException;
import com.fernandocejas.android10.sample.data.moviedb.mapper.MovieDetailsJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.mapper.MovieEntityJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.mapper.VideosEntityJsonMapper;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.MovieResultEntity;
import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.MalformedURLException;

import io.reactivex.Observable;

/**
 * {@link MovieRestApi} implementation for retrieving data from the network.
 */
public class MovieRestApiImpl implements MovieRestApi {

    private final Context context;

    private final MovieEntityJsonMapper movieEntityJsonMapper;

    private final MovieDetailsJsonMapper movieDetailsJsonMapper;

    private final VideosEntityJsonMapper videosEntityJsonMapper;

    private String API_KEY_MOVIEDB = "2b267eb9f6de315a5ef84239d3cb1746";

    public MovieRestApiImpl(Context context, MovieEntityJsonMapper movieEntityJsonMapper,
        MovieDetailsJsonMapper movieDetailsJsonMapper,
        VideosEntityJsonMapper videosEntityJsonMapper) {
        if (context == null || movieEntityJsonMapper == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        this.context = context.getApplicationContext();
        this.movieEntityJsonMapper = movieEntityJsonMapper;
        this.movieDetailsJsonMapper = movieDetailsJsonMapper;
        this.videosEntityJsonMapper = videosEntityJsonMapper;
    }

    @Override
    public Observable<MovieResultEntity> moviePopularList(int page) {
        return Observable.create(emitter -> {
            if (isThereInternetConnection()) {
                try {
                    String responseMovieEntities = getMoviePopularEntitiesFromApi(page);
                    if (responseMovieEntities != null) {
                        emitter.onNext(movieEntityJsonMapper.transformMovieResultEntity(
                            responseMovieEntities));
                        emitter.onComplete();
                    } else {
                        emitter.onError(new NetworkConnectionException());
                    }
                } catch (Exception e) {
                    emitter.onError(new NetworkConnectionException(e.getCause()));
                }
            } else {
                emitter.onError(new NetworkConnectionException());
            }
        });
    }

    @Override
    public Observable<MovieResultEntity> movieTopRatedList(int page) {
        return Observable.create(emitter -> {
            if (isThereInternetConnection()) {
                try {
                    String responseMovieEntities = getMovieTopRatedEntitiesFromApi(page);
                    if (responseMovieEntities != null) {
                        emitter.onNext(movieEntityJsonMapper.transformMovieResultEntity(
                            responseMovieEntities));
                        emitter.onComplete();
                    } else {
                        emitter.onError(new NetworkConnectionException());
                    }
                } catch (Exception e) {
                    emitter.onError(new NetworkConnectionException(e.getCause()));
                }
            } else {
                emitter.onError(new NetworkConnectionException());
            }
        });
    }

    @Override
    public Observable<MovieResultEntity> movieFavoriteList() {
        return null;
    }

    @Override
    public Observable<MovieEntity> movieDetailsById(int movieId) {
        return Observable.create(emitter -> {
            if (isThereInternetConnection()) {
                try {
                    String responseMovieEntities = getMovieDetailsFromApi(movieId);
                    if (responseMovieEntities != null) {
                        emitter.onNext(
                            movieDetailsJsonMapper.transformMovieEntity(responseMovieEntities));
                        emitter.onComplete();
                    } else {
                        emitter.onError(new NetworkConnectionException());
                    }
                } catch (Exception e) {
                    emitter.onError(new NetworkConnectionException(e.getCause()));
                }
            } else {
                emitter.onError(new NetworkConnectionException());
            }
        });
    }

    @Override
    public Observable<VideoResultEntity> videosMovieById(int movieId) {
        return Observable.create(emitter -> {
            if (isThereInternetConnection()) {
                try {
                    String responseMovieEntities = getMovieVideosFromApi(movieId);
                    if (responseMovieEntities != null) {
                        emitter.onNext(videosEntityJsonMapper
                            .transformVideoResultEntity(responseMovieEntities));
                        emitter.onComplete();
                    } else {
                        emitter.onError(new NetworkConnectionException());
                    }
                } catch (Exception e) {
                    emitter.onError(new NetworkConnectionException(e.getCause()));
                }
            } else {
                emitter.onError(new NetworkConnectionException());
            }
        });
    }

    private String getMoviePopularEntitiesFromApi(int page) throws MalformedURLException {
        String apiUrl = API_URL_GET_POPULAR_LIST + "?api_key=2b267eb9f6de315a5ef84239d3cb1746" +
            "&page=" + page;
        return ApiConnection.createGET(apiUrl).requestSyncCall();
    }

    private String getMovieTopRatedEntitiesFromApi(int page) throws MalformedURLException {
        String apiUrl = API_URL_GET_TOP_RATED_LIST + "?api_key=2b267eb9f6de315a5ef84239d3cb1746" +
            "&page=" + page;
        return ApiConnection.createGET(apiUrl).requestSyncCall();
    }

    private String getMovieDetailsFromApi(int movieId) throws MalformedURLException {
        String apiUrl = API_URL_GET_DETAILS_MOVIE + movieId + "?api_key" +
            "=2b267eb9f6de315a5ef84239d3cb1746";
        return ApiConnection.createGET(apiUrl).requestSyncCall();
    }

    private String getMovieVideosFromApi(int movieId) throws MalformedURLException {
        String apiUrl = API_URL_GET_DETAILS_MOVIE + movieId + "/videos?api_key" +
            "=2b267eb9f6de315a5ef84239d3cb1746";
        return ApiConnection.createGET(apiUrl).requestSyncCall();
    }

    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
            (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
            .getNetworkInfo(connectivityManager.getActiveNetwork());
        isConnected = (networkInfo != null && networkInfo.isConnected());

        return isConnected;
    }


}
