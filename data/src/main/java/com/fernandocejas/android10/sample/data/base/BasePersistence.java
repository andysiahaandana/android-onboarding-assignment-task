package com.fernandocejas.android10.sample.data.base;

import com.fernandocejas.android10.sample.data.BuildConfig;

import android.content.Context;

import androidx.room.Room;

public class BasePersistence {

    private static final String DB_NAME = "DB-" + BuildConfig.APPLICATION_ID + "-" + BuildConfig
        .FLAVOR;

    private BasePersistanceDao basePersistanceDao;

    private Context context;

    public BasePersistence(Context context) {
        this.context = context;
    }

    public BasePersistanceDao getDb() {
        if (basePersistanceDao == null) {
            init();
        }
        return basePersistanceDao;
    }

    private void init() {
        basePersistanceDao = Room.databaseBuilder(context, BasePersistanceDao.class, DB_NAME)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build();
    }
}
