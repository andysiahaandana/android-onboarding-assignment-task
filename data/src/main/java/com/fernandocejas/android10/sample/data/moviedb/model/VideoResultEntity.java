package com.fernandocejas.android10.sample.data.moviedb.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version MovieResultEntity, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class VideoResultEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("results")
    private List<VideoEntity> videosList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<VideoEntity> getVideosList() {
        return videosList;
    }
}
