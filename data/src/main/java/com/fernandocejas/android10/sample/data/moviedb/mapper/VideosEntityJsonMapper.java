package com.fernandocejas.android10.sample.data.moviedb.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import com.fernandocejas.android10.sample.data.moviedb.model.VideoResultEntity;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class VideosEntityJsonMapper {

    private final Gson gson;

    @Inject
    public VideosEntityJsonMapper() {
        this.gson = new Gson();
    }

    public VideoResultEntity transformVideoResultEntity(
        String videosJsonResponse) throws JsonSyntaxException {
        final Type videoResultEntity = new TypeToken<VideoResultEntity>() {
        }.getType();
        return this.gson.fromJson(videosJsonResponse, videoResultEntity);
    }
}
