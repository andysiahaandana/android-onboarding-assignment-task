package com.fernandocejas.android10.sample.data.moviedb.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import com.fernandocejas.android10.sample.data.moviedb.model.MovieEntity;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class MovieDetailsJsonMapper {

    private final Gson gson;

    @Inject
    public MovieDetailsJsonMapper() {
        this.gson = new Gson();
    }

    public MovieEntity transformMovieEntity(String movieJsonResponse) throws JsonSyntaxException {
        final Type movieEntity = new TypeToken<MovieEntity>() {
        }.getType();
        return this.gson.fromJson(movieJsonResponse, movieEntity);
    }
}
