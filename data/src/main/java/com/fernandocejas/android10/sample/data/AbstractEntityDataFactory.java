package com.fernandocejas.android10.sample.data;

/**
 * Created by taufiqotulfaidah on 5/31/18.
 *
 * This class is helper of factory class to map the source of data
 */

public abstract class AbstractEntityDataFactory<T> {

    public abstract T createData(@Source String source);
}
