package com.fernandocejas.android10.sample.data.moviedb.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Andy Leonard Siahaan (andy.siahaan@dana.id)
 * @version MovieResultEntity, v 0.1 2019-10-23 23:11 by Andy Leonard Siahaan
 */
public class MovieResultEntity {

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("results")
    private List<MovieEntity> movieList;

    public int getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public List<MovieEntity> getMovies() {
        return movieList;
    }
}
